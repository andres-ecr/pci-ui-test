export const dateFormatter = (params: { value: string | null }): string => {
  if (!params.value) return '';
  const date = new Date(params.value);

  return date.toLocaleDateString('en-US', {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
  });
};

export const phaValueGetter = (params: { data: { pha: string } }): string => {
  if (params.data.pha === 'Y') return 'Yes';
  if (params.data.pha === 'N') return 'No';
  return '';
};

export const copySelectedRowsToClipboard = (gridApi: any): void => {
  if (!gridApi) return;

  const selectedNodes = gridApi.getSelectedNodes();
  const clipboardData = selectedNodes
    .map((node: any) => {
      return Object.values(node.data).join('\t');
    })
    .join('\n');

  navigator.clipboard.writeText(clipboardData);
};

export const clearFiltersAndSorters = (
  gridApi: any,
  gridColumnApi: any
): void => {
  if (gridApi && gridColumnApi) {
    gridApi.setFilterModel(null);

    gridColumnApi.applyColumnState({
      defaultState: {
        sort: null,
      },
    });
  }
};
