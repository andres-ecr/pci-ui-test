import { ColDef } from 'ag-grid-community';
import { dateFormatter, phaValueGetter } from '../functions';

export const columnDefs: ColDef[] = [
  {
    field: 'designation',
    headerName: 'Designation',
    sortable: true,
    filter: 'agTextColumnFilter',
  },
  {
    field: 'discovery_date',
    headerName: 'Discovery Date',
    sortable: true,
    valueFormatter: dateFormatter,
  },
  {
    field: 'h_mag',
    headerName: 'H (mag)',
    sortable: true,
    filter: 'agNumberColumnFilter',
    valueGetter: (params) => {
      const value = parseFloat(params.data.h_mag);
      return isNaN(value) ? null : value;
    },
  },
  {
    field: 'moid_au',
    headerName: 'MOID (au)',
    sortable: true,
    filter: 'agNumberColumnFilter',
    valueGetter: (params) => {
      const value = parseFloat(params.data.moid_au);
      return isNaN(value) ? null : value;
    },
  },
  {
    field: 'q_au_1',
    headerName: 'q (au)',
    sortable: true,
    filter: 'agNumberColumnFilter',
    valueGetter: (params) => {
      const value = parseFloat(params.data.q_au_1);
      return isNaN(value) ? null : value;
    },
  },
  {
    field: 'q_au_2',
    headerName: 'Q (au)',
    sortable: true,
    filter: 'agNumberColumnFilter',
    valueGetter: (params) => {
      const value = parseFloat(params.data.q_au_2);
      return isNaN(value) ? null : value;
    },
  },
  {
    field: 'period_yr',
    headerName: 'Period (yr)',
    sortable: true,
    filter: 'agNumberColumnFilter',
    valueGetter: (params) => {
      const value = parseFloat(params.data.period_yr);
      return isNaN(value) ? null : value;
    },
  },
  {
    field: 'i_deg',
    headerName: 'Inclination (deg)',
    sortable: true,
    filter: 'agNumberColumnFilter',
    valueGetter: (params) => {
      const value = parseFloat(params.data.i_deg);
      return isNaN(value) ? null : value;
    },
  },
  {
    field: 'pha',
    headerName: 'Potentially Hazardous',
    sortable: true,
    filter: 'agTextColumnFilter',
    valueGetter: phaValueGetter,
  },
  {
    field: 'orbit_class',
    headerName: 'Orbit Class',
    sortable: true,
    filter: 'agTextColumnFilter',
    enableRowGroup: true,
    minWidth: 200,
  },
];
