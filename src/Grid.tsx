import { useState, useEffect } from 'react';

// Ag-Grid
import { AgGridReact } from 'ag-grid-react';
import { GridReadyEvent, GridApi, ColumnApi } from 'ag-grid-community';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

// Data
import data from './data/near-earth-asteroids.json';

// Utils
import {
  clearFiltersAndSorters,
  copySelectedRowsToClipboard,
} from './utils/functions';
import { columnDefs } from './utils/columnDefinitions';
import HeaderBar from './components/HeaderBar';

const NeoGrid = (): JSX.Element => {
  const [gridApi, setGridApi] = useState<GridApi | null>(null);
  const [gridColumnApi, setGridColumnApi] = useState<ColumnApi | null>(null);

  // Set gridApi onGridReady and size columns to fit
  const onGridReady = (params: GridReadyEvent) => {
    setGridApi(params.api);
    setGridColumnApi(params.columnApi);
    params.api.sizeColumnsToFit();
  };

  // Add Ctrl+C listener to copy selected rows to clipboard
  useEffect(() => {
    const handleKeyPress = (event: KeyboardEvent) => {
      if (event.ctrlKey && event.key === 'c') {
        copySelectedRowsToClipboard(gridApi);
      }
    };

    document.addEventListener('keydown', handleKeyPress);

    return () => {
      document.removeEventListener('keydown', handleKeyPress);
    };
  }, [gridApi]);

  return (
    <div className="ag-theme-alpine" style={{ height: 900, width: 1920 }}>
      <HeaderBar
        onClear={() => clearFiltersAndSorters(gridApi, gridColumnApi)}
      />
      <AgGridReact
        rowData={data}
        columnDefs={columnDefs}
        rowGroupPanelShow={'always'}
        onGridReady={onGridReady}
        rowSelection="multiple"
      />
    </div>
  );
};

export default NeoGrid;
