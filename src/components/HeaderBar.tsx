const HeaderBar = ({ onClear }: { onClear: () => void }) => (
  <div style={{ display: 'flex', alignItems: 'center', marginBottom: '15px' }}>
    <h1 style={{ marginRight: '15px' }}>Near-Earth Object Overview</h1>
    <button onClick={onClear}>Clear Filters and Sorters</button>
  </div>
);

export default HeaderBar;
